import React from 'react';
import s from './Header.module.css';
import {keycloak} from "../../index";

const logout = () => {
    keycloak.logout()
}
const Header = () => {
    return <header className={s.header}>
        <img src='https://www.freelogodesign.org/Content/img/logo-ex-7.png'
             alt=''/>
        <button onClick={logout}>logout</button>
    </header>
}

export default Header;