import Users from "./Users";
import {connect} from "react-redux";
import {followAC, setUserAC, unFollowAC} from "../../redux/UserReducer";

let mapStateToProps = (state) => {
    return {
        users: state.usersPage.users
    }
}
let mapDispatchToProps = (dispatch) => {
    return {
        follow: (userId) => {
            dispatch(followAC(userId))
        },
        unfollow: (userId) => {
            dispatch(unFollowAC(userId))
        },
        setUsers: (users) => {
            dispatch(setUserAC(users))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)