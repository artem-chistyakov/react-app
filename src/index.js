import store from "./redux/ReduxStore";
import React from 'react';
import {render} from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import Keycloak from "keycloak-js";
import {Provider} from "react-redux";
import * as serviceWorker from './serviceWorker';

export const keycloak = Keycloak('/keycloak.json');

keycloak.init({onLoad: 'login-required'}).then((auth) => {

    if (!auth) {
        window.location.reload();
    } else {
        console.info("Authenticated");
    }
    render(
        <BrowserRouter>
            <Provider store={store}>
                <App/>
            </Provider>
        </BrowserRouter>
        , document.getElementById('root')
    )

    localStorage.setItem("keycloak-token", keycloak.token);
}).catch(() => {
    console.error("Authenticated Failed");
});

serviceWorker.unregister();